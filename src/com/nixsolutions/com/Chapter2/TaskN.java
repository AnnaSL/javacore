package com.nixsolutions.com.Chapter2;

public class TaskN {

	public boolean booleanField;
	public char charField;
	public byte byteField;
	public short shortField;
	public int intField;
	public long longField;
	public float floatField;
	public double doubleField;

	public static void main(String[] args) {

		TaskN taskN = new TaskN();
		System.out.println("boolean = " + taskN.booleanField);
		System.out.println("char = " + taskN.charField);
		System.out.println("byte = " + taskN.byteField);
		System.out.println("short = " + taskN.shortField);
		System.out.println("int = " + taskN.intField);
		System.out.println("long = " + taskN.longField);
		System.out.println("float = " + taskN.floatField);
		System.out.println("double = " + taskN.doubleField);
	}
}
