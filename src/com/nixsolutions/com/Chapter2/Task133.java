package com.nixsolutions.com.Chapter2;

import java.util.*;

// object/Documentation3.java
/**
 * You can even insert a list:
 * <ol>
 * <li> Item one
 * <li> Item two
 * <li> Item three
 * </ol>
 */

public class Task133 {
    public static void main(String[] args) {
        Date d = new Date();
        System.out.println("d = " + d);
    }
}

