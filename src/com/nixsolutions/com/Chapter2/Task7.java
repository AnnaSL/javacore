package com.nixsolutions.com.Chapter2;

class StaticTest {
    static int i = 47;
}

class Incrementable {
    static void increment(){
        StaticTest.i++;
    }
}

public class Task7 {
    public static void main(String[] args) {
        System.out.println("StaticTest.i: " + StaticTest.i);
        StaticTest s1 = new StaticTest();
        StaticTest s2 = new StaticTest();
        System.out.println("s1.i: " + s1.i);
        System.out.println("s2.i: " + s2.i);
        Incrementable.increment();
        System.out.println("After Incrementable.increment() called: ");
        System.out.println("s1.i: " + s1.i);
        System.out.println("s2.i: " + s2.i);
        Incrementable incrementable = new Incrementable();
        incrementable.increment();
        System.out.println("After incrementable.increment(); called: ");
        System.out.println("s1.i: " + s1.i);
        System.out.println("s2.i: " + s2.i);
        System.out.println("StaticTest.i: " + StaticTest.i);
    }
}

