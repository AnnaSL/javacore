package com.nixsolutions.com.Chapter2;

public class DataOnly45 {
    int i;
    double d;
    boolean b;

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public boolean isB() {
        return b;
    }

    public void setB(boolean b) {
        this.b = b;
    }

    public static void main(String[] args) {
        DataOnly45 data = new DataOnly45();
        data.setI(5);
        data.setD(22.5d);
        data.setB(true);

        System.out.println(data.getI());
        System.out.println(data.getD());
        System.out.println(data.isB());

    }
}
