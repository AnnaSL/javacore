package com.nixsolutions.com.Chapter2;


/**
 * Public class contained in file of the same name that includes main()
 */

public class Task15 {

    /** main method executed by java
     */

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
