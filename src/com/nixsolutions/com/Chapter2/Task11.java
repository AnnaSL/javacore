package com.nixsolutions.com.Chapter2;

public class Task11 {
    public static void main(String[] args) {
        AllTheColorsOfTheRainbow atc = new AllTheColorsOfTheRainbow();
        System.out.println("atc.anIntegerRepresentingColors = " + atc.anIntegerRepresentingColors);
        atc.setAnIntegerRepresentingColors(5);
        atc.setHue(15);
        System.out.println("After color change, atc.anIntegerRepresentingColors = " + atc.anIntegerRepresentingColors);
        System.out.println("atc.hue = " + atc.hue);
    }
}

class AllTheColorsOfTheRainbow {
    int anIntegerRepresentingColors = 0;
    int hue = 0;

    public int getAnIntegerRepresentingColors() {
        return anIntegerRepresentingColors;
    }

    public void setAnIntegerRepresentingColors(int anIntegerRepresentingColors) {
        this.anIntegerRepresentingColors = anIntegerRepresentingColors;
    }

    public int getHue() {
        return hue;
    }

    public void setHue(int hue) {
        this.hue = hue;
    }

}
