package com.nixsolutions.com.Chapter2;

import java.util.*;

// object/Documentation2.java
/**
 * <pre>
 * Uses
 * System.out.println(new Date());
 * </pre>
 */

public class Task131 {
    Date d = new Date();
    void showDate() {
        System.out.println("Date = " + d);
    }
}
