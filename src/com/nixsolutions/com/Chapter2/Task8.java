package com.nixsolutions.com.Chapter2;

class StaticTestTask8 {
    static int i = 10;
}

class IncrementableTask8 {
    static void increment() {
        StaticTestTask8.i++;
    }
}
public class Task8 {
    public static void main(String[] args) {
        System.out.println("StaticTestTask8.i: " +  StaticTestTask8.i);
        StaticTestTask8 s1 = new StaticTestTask8();
        StaticTestTask8 s2 = new StaticTestTask8();
        System.out.println("s1: " +  s1.i);
        System.out.println("s2: " +  s2.i);
        IncrementableTask8.increment();
        System.out.println("After IncrementableTask8.increment(); called ");
        System.out.println("s1: " +  s1.i);
        System.out.println("s2: " +  s2.i);
        IncrementableTask8 incrementableTask8 = new IncrementableTask8();
        incrementableTask8.increment();
        System.out.println("After incrementableTask8.increment(); called ");
        System.out.println("s1: " +  s1.i);
        System.out.println("s2: " +  s2.i);
        s1.i = 5;
        System.out.println("After s1.i = 5; ");
        System.out.println("s1: " +  s1.i);
        System.out.println("s2: " +  s2.i);
        IncrementableTask8.increment();
        System.out.println("After IncrementableTask8.increment(); called ");
        System.out.println("s1: " +  s1.i);
        System.out.println("s2: " +  s2.i);
        StaticTestTask8 s3 = new StaticTestTask8();
        System.out.println("After new object has been created ");
        System.out.println("s1: " +  s1.i);
        System.out.println("s2: " +  s2.i);
        System.out.println("s3: " +  s3.i);
    }
}
