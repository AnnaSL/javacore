package com.nixsolutions.com.Chapter2;


public class Task9 {
    public static void main(String[] args) {
        boolean b = true;
        char c = 'a';
        byte bt = 8;
        short sh = 16;
        int i = 32;
        long l = 64;
        float f = 0.32f;
        double d = 0.64d;

        Boolean B = b;
        System.out.println("boolean b = " + b);
        System.out.println("Boolean B = " + B);

        Character C = c;
        System.out.println("char c = " + c);
        System.out.println("Character C = " + C);

        Byte Bt = bt;
        System.out.println("byte b = " + bt);
        System.out.println("Byte B = " + Bt);

        Short Sh = sh;
        System.out.println("short sh = " + sh);
        System.out.println("Short Sh = " + Sh);

        Integer I = i;
        System.out.println("int i = " + i);
        System.out.println("Integer I = " + I);

        Long L = l;
        System.out.println("long l = " + l);
        System.out.println("Long L = " + L);

        Float F = f;
        System.out.println("float f = " + f);
        System.out.println("Float F = " + F);

        Double D = d;
        System.out.println("double d = " + d);
        System.out.println("Double D = " + D);

    }
}
